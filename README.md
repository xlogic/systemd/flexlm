# FlexLM

<!--
SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
SPDX-License-Identifier: Apache-2.0
SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>
-->

[[_TOC_]]

## Description

Using containerized FlexLM license managers as systemd service units for serving
FPGA/ASIC EDA software licenses that can be also used in CI/CD flow.

## Features

- Configurable single systemd service template unit that can serve licenses for different licensed EDA software's
  (`questa`, `quartus`, `vivado`, ...)
- Deploy to any cloud provider like Google Cloud and use any licensed EDA software's in your CI/CD flow
- Running license manager in containerized environment via `podman` (preferable) or `docker`
- Automatically retrieves MAC address from provided license file and use it for containerized FlexLM instance
- Automatically retrieves hostname from provided license file and use it for containerized FlexLM instance
- Automatically overrides MAC address in provided license file from started FlexLM container
- Automatically overrides hostname in provided license file from started FlexLM container
- Automatically sets hostname, MAC address, service port, vendor daemon executable, path and port in provided license files
- Configurable service using environment variables via optional environment file
- Possibility to publish container ports to serve licenses like any normal server
- Possibility to run FlexLM systemd service unit in system mode (`--system`) or user mode (`--user`)
- Running smoothly and out-of-box on any machine platform

## Install

### User Mode

To install FlexLM license manager systemd service unit in user mode:

```plaintext
curl -s "https://gitlab.com/xlogic/systemd/flexlm/-/raw/master/install.sh" | sh -s -- --user
```

This can be also done locally after cloning this git project repository:

```plaintext
./install.sh --user
```

In next commands the `<vendor>-<tool>` part is systemd service template argument and it is used to specialize
systemd service template for specific EDA software. For example setting it to `questa` specialize
FlexLM systemd service template to serve licenses for Questa*-Intel® FPGA Edition Software.

Create directory for license files. Replace the `<vendor>` part with `intel`, `xilinx`, etc. and
the `<tool>` part with `questa`, `quartus`, `vivado` etc.:

```plaintext
mkdir -p ~/.config/xlogic/flexlm/<vendor>/<tool>/licenses
```

Generate and download license file from specific vendor license center site and copy it to that created directory:

```plaintext
cp ~/Downloads/*_License.dat ~/.config/xlogic/flexlm/<vendor>/<tool>/licenses/
```

Enable specialized FlexLM systemd service unit:

```plaintext
systemctl --user enable xlogic-flexlm@<vendor>-<tool>
```

Start specialized FlexLM systemd service unit:

```plaintext
systemctl --user start xlogic-flexlm@<vendor>-<tool>
```

Check status of started specialized FlexLM systemd service unit:

```plaintext
systemctl --user status xlogic-flexlm@<vendor>-<tool>
```

:warning: Note that started systemd service unit in user mode runs only during active user session.

### System Mode

System mode requires to run commands via `sudo` or be logged as `root` user.

To install FlexLM license manager systemd service unit in system mode:

```plaintext
curl -s "https://gitlab.com/xlogic/systemd/flexlm/-/raw/master/install.sh" | sudo sh -s -- --system
```

This can be also done locally after cloning this git project repository:

```plaintext
sudo ./install.sh --system
```

Create directory for license files. Replace the `<vendor>` part with `intel`, `xilinx`, etc. and
the `<tool>` part with `questa`, `quartus`, `vivado` etc.:

```plaintext
sudo mkdir -p /etc/xlogic/flexlm/<vendor>/<tool>/licenses
```

Generate and download license file from specific vendor license center site and copy it to that created directory:

```plaintext
sudo cp ~/Downloads/*_License.dat /etc/xlogic/flexlm/<vendor>/<tool>/licenses/
```

Enable specialized FlexLM systemd service unit:

```plaintext
sudo systemctl --system enable xlogic-flexlm@<vendor>-<tool>
```

Start specialized FlexLM systemd service unit:

```plaintext
sudo systemctl --system start xlogic-flexlm@<vendor>-<tool>
```

Check status of started specialized FlexLM systemd service unit:

```plaintext
sudo systemctl --system status xlogic-flexlm@<vendor>-<tool>
```

## Configuration

Sets environment variables in environment file `xlogic/flexlm/<vendor>/<tool>/env` to configure
FlexLM license manager systemd service unit.

For system mode:

```plaintext
/etc/xlogic/flexlm/<vendor>/<tool>/env
```

For user mode:

```plaintext
~/.config/xlogic/flexlm/<vendor>/<tool>/env
```

### Environment Variables

If not specified, each environment variable has own default value that can be overridden using
environment file `xlogic/flexlm/<vendor>/<tool>/env` or systemd service drop-ins.

| Name                           | Default                                                | Description             |
| :----------------------------- | :----------------------------------------------------- | :---------------------- |
| `XLOGIC_CONTAINER_CMD`         | `podman` (preferable) or `docker`                      | Container command       |
| `XLOGIC_CONTAINER_NAME`        | `xlogic-flexlm-<vendor>-<tool>`                        | Container name          |
| `XLOGIC_CONTAINER_IMAGE`       | `registry.gitlab.com/xlogic/tool/flex/<vendor>/<tool>` | Container image         |
| `XLOGIC_CONTAINER_TAG`         | `latest`                                               | Container image tag     |
| `XLOGIC_CONTAINER_VOLUME`      | `xlogic-flexlm-<vendor>-<tool>-licenses`               | Container volume name for license files |
| `XLOGIC_CONTAINER_NETWORK`     | `xlogic`                                               | Container network name  |
| `XLOGIC_CONTAINER_HOSTNAME`    | `xlogic-flexlm-<vendor>-<tool>` or from license file   | Container hostname      |
| `XLOGIC_CONTAINER_MAC_ADDRESS` | From provided license file                             | Container MAC address in `xx:xx:xx:xx:xx:xx` format |
| `XLOGIC_CONTAINER_PUBLISH`     |                                                        | Publish container ports. Separated using spaces |
