#!/usr/bin/env sh
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>
#
# shellcheck disable=SC2310

set -e

PROJECT_DIR="$(cd -- "$(dirname "$0")" >/dev/null 2>&1; pwd -P)"
URL="https://gitlab.com/xlogic/systemd/flexlm/-/raw"
REF="master"
SYSTEM=""

print_help() {
    cat <<-EOF
Usage: $0 [OPTIONS]

It installs systemd FlexLM license manager service.

Options:
  -h, --help        it prints this help message
EOF
}

error() {
    >&2 echo "[ERROR]: $*"
}

fatal() {
    error "$*. Exiting..."; exit 1
}

parse_command_line_arguments() {
    while [ "$#" -ne 0 ]; do
        case "$1" in
            -h|--help)
                print_help; exit 0;;
            -u|--user)
                SYSTEM="";;
            -s|--system)
                SYSTEM="1";;
            *)
                fatal "Unknown argument: $1";;
        esac

        shift
    done
}

command_exists() {
    command -v "$1" >/dev/null 2>&1
}

create_directory() {
    if [ -d "$1" ]; then
        return 0
    fi

    echo "Creating directory: $1"
    mkdir -p "$1"
    chmod 750 "$1"
    chown "${USER}:${USER}" "$1"
}

main() {
    parse_command_line_arguments "$@"

    if [ "${USER:-}" = "root" ]; then
        SYSTEM="1"
    fi

    if [ -z "${SYSTEM:-}" ]; then
        SCRIPT_DIR="${HOME:-/home/${USER}}/.local/bin"
        CONFIG_DIR="${XDG_CONFIG_HOME:-${HOME:-/home/${USER}}/.config}/xlogic/flexlm"
        SERVICE_DIR="${XDG_CONFIG_HOME:-${HOME:-/home/${USER}}/.config}/systemd/user"
    else
        USER="root"
        SCRIPT_DIR="/root/.local/bin"
        CONFIG_DIR="/etc/xlogic/flexlm"
        SERVICE_DIR="/etc/systemd/system"
    fi

    create_directory "${CONFIG_DIR}"

    for file in "${SCRIPT_DIR}/xlogic-flexlm.sh" "${SERVICE_DIR}/xlogic-flexlm@.service"; do
        dir="$(dirname "${file}")"
        name="$(basename "${file}")"

        create_directory "${dir}"

        if [ -f "${file}" ]; then
            echo "Updating file: ${file}"
        else
            echo "Installing file: ${file}"
        fi

        if [ -f "${PROJECT_DIR}/${name}" ]; then
            cp "${PROJECT_DIR}/${name}" "${file}"
        elif command_exists curl; then
            curl -s -o "${file}" "${URL}/${REF}/${name}"
        elif command_exists wget; then
            wget -qO "${file}" "${URL}/${REF}/${name}"
        else
            fatal "curl or wget are not installed"
        fi

        case "${name}" in
            *.sh)
                chmod 750 "${file}";;
            *)
                chmod 640 "${file}";;
        esac

        chown "${USER}:${USER}" "${file}"
    done
}

main "$@"
