#!/usr/bin/env sh
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>
#
# shellcheck disable=SC2310

set -e

COMMAND=""
NAME=""
VENDOR=""
TOOL=""

print_help() {
    cat <<-EOF
Usage: $0 [OPTIONS] COMMAND NAME

It runs or stops FlexLM license manager service with selected vendor daemon for licensing specific software.

COMMAND:
    run             it runs FlexLM license manager service in foreground
    stop            it stops running FlexLM license manager service

NAME is vendor daemon name used by FlexLM license manager for licensing specific software like questa or quartus.

Options:
  -h, --help        it prints this help message
EOF
}

error() {
    >&2 echo "[ERROR]: $*"
}

fatal() {
    error "$*. Exiting..."; exit 1
}

normalize() {
    echo "$1" | tr '[:upper:]' '[:lower:]'
}

path() {
    echo "$1" | tr '-' '/'
}

parse_command_line_arguments() {
    while [ "$#" -ne 0 ]; do
        case "$1" in
            -h|--help)
                print_help; exit 0;;
            -*)
                fatal "Unknown argument: $1";;
            *)
                if [ -z "${COMMAND}" ]; then
                    COMMAND="$(normalize "$1")"
                elif [ -z "${NAME}" ]; then
                    NAME="$(normalize "$1")"
                    VENDOR="$(echo "${NAME}" | cut -d '-' -f 1)"
                    TOOL="$(echo "${NAME}" | cut -d '-' -f 2-)"
                else
                    fatal "Too much arguments"
                fi;;
        esac

        shift
    done

    if [ -z "${COMMAND}" ]; then
        fatal "Command was not specified"
    fi

    if [ -z "${NAME}" ]; then
        fatal "Name was not specified"
    fi
}

command_exists() {
    command -v "$1" >/dev/null 2>&1
}

detect_container_command() {
    if [ -n "${XLOGIC_CONTAINER_CMD:-}" ]; then
        return 0
    fi

    for cmd in "$@"; do
        if command_exists "${cmd}"; then
            XLOGIC_CONTAINER_CMD="${cmd}"
            return 0
        fi

        if command_exists "/usr/bin/${cmd}"; then
            XLOGIC_CONTAINER_CMD="/usr/bin/${cmd}"
            return 0
        fi
    done

    fatal "It seems that one of container executable is not installed or available: $*"
}

read_hostname() {
    sed -nE 's/^\s*SERVER\s+([[:graph:]]+)\s+.*$/\1/Ip' "$1"
}

read_mac_address() {
    sed -nE 's/^\s*SERVER\s+[[:graph:]]+\s+([[:graph:]]+).*$/\1/Ip' "$1" | \
        tr -d ':' | tr '[:upper:]' '[:lower:]' | sed 's/..\B/&:/g'
}

get_hostname() {
    for license in "${XDG_CONFIG_HOME}/xlogic/flexlm/${VENDOR}/${TOOL}/licenses"/*; do
        hostname="$(read_hostname "${license}")"

        if echo "${hostname}" | grep -qE '[^a-zA-Z0-9.-]'; then
            echo "${XLOGIC_CONTAINER_NAME:-xlogic-flexlm-${NAME}}"
            return 0
        fi

        if [ -n "${hostname}" ]; then
            echo "${hostname}"
            return 0
        fi
    done
}

get_mac_address() {
    for license in "${XDG_CONFIG_HOME}/xlogic/flexlm/${VENDOR}/${TOOL}/licenses"/*; do
        mac_address="$(read_mac_address "${license}")"

        if [ -n "${mac_address}" ]; then
            echo "${mac_address}"
            return 0
        fi
    done
}

expand() {
    argument="$1"; shift

    for item in "$@"; do
        printf ' %s %s' "${argument}" "${item}"
    done
}

create_network() {
    if "${XLOGIC_CONTAINER_CMD}" network inspect "$1" >/dev/null 2>&1; then
        return 0
    fi

    "${XLOGIC_CONTAINER_CMD}" network create ${XLOGIC_CONTAINER_IGNORE:+--ignore} "$1"
}

create_volume() {
    if "${XLOGIC_CONTAINER_CMD}" volume inspect "$1" >/dev/null 2>&1; then
        return 0
    fi

    mkdir -p "$2"

    "${XLOGIC_CONTAINER_CMD}" volume create ${XLOGIC_CONTAINER_IGNORE:+--ignore} \
        --driver local \
        --opt type=none \
        --opt o=bind \
        --opt device="$2" \
        "$1"
}

command_run() {
    create_network "${XLOGIC_CONTAINER_NETWORK}"
    create_volume  "${XLOGIC_CONTAINER_VOLUME}" "${XDG_CONFIG_HOME}/xlogic/flexlm/${VENDOR}/${TOOL}/licenses"

    publish="$(expand --publish ${XLOGIC_CONTAINER_PUBLISH:+${XLOGIC_CONTAINER_PUBLISH:-}})"

    exec "${XLOGIC_CONTAINER_CMD}" run \
        --rm \
        ${publish:+${publish:-}} \
        ${XLOGIC_CONTAINER_KEEP_ID:+--userns=keep-id} \
        ${XLOGIC_CONTAINER_NETWORK:+--network "${XLOGIC_CONTAINER_NETWORK}"} \
        ${XLOGIC_CONTAINER_HOSTNAME:+--hostname "${XLOGIC_CONTAINER_HOSTNAME}"} \
        ${XLOGIC_CONTAINER_MAC_ADDRESS:+--mac-address "${XLOGIC_CONTAINER_MAC_ADDRESS}"} \
        ${XLOGIC_CONTAINER_VOLUME:+--volume "${XLOGIC_CONTAINER_VOLUME}:/var/flexlm/licenses:ro,z"} \
        --name "$1" \
        "${XLOGIC_CONTAINER_IMAGE}:${XLOGIC_CONTAINER_TAG}"
}

command_stop() {
    if ! "${XLOGIC_CONTAINER_CMD}" container inspect "$1" >/dev/null 2>&1; then
        return 0
    fi

    "${XLOGIC_CONTAINER_CMD}" stop ${XLOGIC_CONTAINER_IGNORE:+--ignore} "$1"
    "${XLOGIC_CONTAINER_CMD}" rm ${XLOGIC_CONTAINER_IGNORE:+--ignore} ${XLOGIC_CONTAINER_FORCE:+--force} "$1"
}

main() {
    parse_command_line_arguments "$@"
    detect_container_command podman docker

    case "${USER:-root}" in
        root)
            XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-/etc}"
            ;;
        *)
            XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-${HOME:-/home/${USER}}/.config}"
            ;;
    esac

    case "${XLOGIC_CONTAINER_CMD}" in
        podman)
            XLOGIC_CONTAINER_FORCE=1
            XLOGIC_CONTAINER_IGNORE=1
            XLOGIC_CONTAINER_KEEP_ID=1
            ;;
        *)
            XLOGIC_CONTAINER_FORCE=""
            XLOGIC_CONTAINER_IGNORE=""
            XLOGIC_CONTAINER_KEEP_ID=""
            ;;
    esac

    XLOGIC_CONTAINER_NAME="${XLOGIC_CONTAINER_NAME:-xlogic-flexlm-${NAME}}"
    XLOGIC_CONTAINER_VOLUME="${XLOGIC_CONTAINER_VOLUME:-xlogic-flexlm-${NAME}-licenses}"
    XLOGIC_CONTAINER_NETWORK="${XLOGIC_CONTAINER_NETWORK:-xlogic}"
    XLOGIC_CONTAINER_IMAGE="${XLOGIC_CONTAINER_IMAGE:-registry.gitlab.com/xlogic/tool/flexlm/${VENDOR}/${TOOL}}"
    XLOGIC_CONTAINER_TAG="${XLOGIC_CONTAINER_TAG:-latest}"
    XLOGIC_CONTAINER_MAC_ADDRESS="${XLOGIC_CONTAINER_MAC_ADDRESS:-$(get_mac_address)}"
    XLOGIC_CONTAINER_HOSTNAME="${XLOGIC_CONTAINER_HOSTNAME:-$(get_hostname)}"

    case "${COMMAND}" in
        run)
            command_run "${XLOGIC_CONTAINER_NAME}";;
        stop)
            command_stop "${XLOGIC_CONTAINER_NAME}";;
        *)
            fatal "Provided command doesn't exist: ${COMMAND}";;
    esac
}

main "$@"
